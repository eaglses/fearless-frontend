import React, {useEffect, useState} from 'react';


function PresentationForm  (props) {
    
    const [conferences, setConferences] = useState([]);
    const [conferenceHref, setConference] = useState('');
    const [conferenceName, setConferenceName] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [title, setTitle] = useState('');
    const [company_name, setCompanyName] = useState('');
    const [presenter_email, setPresenterEmail] = useState('');
    const [presenter_name, setPresenterName] = useState('');



      const handleConference = (event) => {
        const selectedIndex = event.target.options.selectedIndex;
        const href = (event.target.options[selectedIndex].getAttribute('value'));
        const name = (event.target.options[selectedIndex].getAttribute('conferenceName'));
        setConference(href);
        setConferenceName(name);
      }
      const handleSynopsis = (event) => {
        const value = event.target.value;
        setSynopsis(value);
      }
      const handleTitle = (event) => {
        const value = event.target.value;
        setTitle(value);
      }
      const handleCompanyName = (event) => {
        const value = event.target.value;
        setCompanyName(value);
      }
      const handlePresenterEmail = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
      }
      const handlePresenterName = (event) => {
        const value = event.target.value;
        setPresenterName(value);
      }

    const handleSubmit = async (event) => {
        event.preventDefault();
  
        const data = {};
  
        data.presenter_name = presenter_name;
        data.company_name = company_name;
        data.presenter_email = presenter_email;
        data.title = title;
        data.synopsis = synopsis;        
        data.conference = {
            "href": conferenceHref,
            "name": conferenceName,
        }
        
        
    
        
        const locationUrl = `http://localhost:8000${conferenceHref}presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);

        

        if (response.ok) {
            const newLocation = await response.json();
          
        
        
            setConference('');
            setConferenceName('');
            setSynopsis('')
            setTitle('')
            setCompanyName('')
            setPresenterEmail('')
            setPresenterName('')
      
          }
    }
  

      

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
       const data = await response.json();
       setConferences(data.conferences);
    
      
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(

      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="htmlForm-floating mb-3">
              <input onChange={handlePresenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" value={presenter_name} />
              </div>
              <div className="htmlForm-floating mb-3">
              <input onChange={handlePresenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" value={presenter_email} />
              </div>
              <div className="htmlForm-floating mb-3">
              <input onChange={handleCompanyName} placeholder="Company name" required type="text" name="company_name" id="company_name" className="form-control" value={company_name} />
              </div>
              <div className="htmlForm-floating mb-3">
              <input onChange={handleTitle} placeholder="title" required type="text" name="title" id="title" className="form-control" value={title} />
              </div>
              <div className="htmlForm-floating mb-3">
              <input onChange={handleSynopsis} placeholder="synopsis" required type="text" name="synopsis" id="synopsis" className="form-control" value={synopsis} />
              </div>
              
              <div className="mb-3">
                <select onChange={handleConference} required name="conference" id="conference" className="htmlForm-select" value={conferenceHref} >
                  <option value="">Choose a locations</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href} conferenceName={conference.name}>
                            {conference.name}
                        </option>
                        );
                    })}
                 </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    );
}

export default PresentationForm;