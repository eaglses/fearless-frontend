import React, {useEffect, useState} from 'react';

function hrefLocationToId(href) {return  href.slice(15, -1)}

function ConferenceForm (props) {
    
     const [locations, setLocations] = useState([]);
     const [location, setLocation] = useState('');
     const [locationId, setLocationId] = useState('');
     const [name, setName] = useState('');
     const [starts, setStarts] = useState('');
     const [ends, setEnds] = useState('');
     const [description, setDescription] = useState('');
     const [maximumPresentations, setMaximumPresentations] = useState('');
     const [maximumAttendees, setMaximumAttendees] = useState('');


     const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }
      const handleLocation = (event) => {
        const selectedIndex = event.target.options.selectedIndex;
        const href = (event.target.options[selectedIndex].getAttribute('value'));
        const name = (event.target.options[selectedIndex].getAttribute('locationName'));
        const id = hrefLocationToId(href);
        setLocation(href);
        setLocationId(id);
      }
      const handleStart = (event) => {
        const value = event.target.value;
        setStarts(value);
      }
      const handleEnds = (event) => {
        const value = event.target.value;
        setEnds(value);
      }
      const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
      }
      const handleMaximumPresentations = (event) => {
        const value = event.target.value;
        setMaximumPresentations(value);
      }
      const handleMaximumAttendees = (event) => {
        const value = event.target.value;
        setMaximumAttendees(value);
      }

    const handleSubmit = async (event) => {
        event.preventDefault();
  
        const data = {};
  
        data.location = locationId;
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maximumPresentations;
        data.max_attendees = maximumAttendees;
    
        
    

        
        
        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);

        

        if (response.ok) {
            const newLocation = await response.json();
      
        
            setName('');
            setStarts('');
            setEnds('');
            setDescription('')
            setMaximumPresentations('')
            setMaximumAttendees('')
            setLocation('')
            
            
          }
    }
  

      

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
       const data = await response.json();
      setLocations(data.locations);
   
      
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="htmlForm-floating mb-3">
              <label htmlFor="name">Name</label>
              <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
              </div>
              <div className="htmlForm-floating mb-3">
              <label htmlFor="starts">starts</label>
              <input onChange={handleStart} placeholder="starts" required type="date" name="starts" id="starts" className="form-control" value={starts} />
              </div>
              <div className="htmlForm-floating mb-3">
              <label  htmlFor="ends">ends</label>
              <input onChange={handleEnds} placeholder="ends" required type="date" name="ends" id="ends" className="form-control" value={ends} />
              </div>
              <div className="htmlForm-floating mb-3">
              <label htmlFor="description">Description</label>
              <input onChange={handleDescription} placeholder="Description" required type="text" name="description" id="description" className="form-control" value={description} />
              </div>
              <div className="htmlForm-floating mb-3">
                <input onChange={handleMaximumPresentations} placeholder="Maximum presentations" required type="number" name="maximum_presentations" id="maximum_presentations" className="form-control" value={maximumPresentations} />  
              </div>
              <div className="htmlForm-floating mb-3">
                <input onChange={handleMaximumAttendees} placeholder="Maximum attendees" required type="number" name="maximum_attendees" id="maximum_attendees" className="form-control" value={maximumAttendees} />  
              </div>
              
              <div className="mb-3">
                <select onChange={handleLocation} required name="location" id="location" className="htmlForm-select" value={location} >
                  <option value="">Choose a locations</option>
                  {locations.map(location => {
                    return (
                        <option key={location.href} value={location.href} locationName={location.name}>
                            {location.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    );
}

export default ConferenceForm;