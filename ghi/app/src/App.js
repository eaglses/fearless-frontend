import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './new-conference'
import AttendeeSignUp from './attendeeSignUp'
import PresentationForm from './PresentationForm'
import MainPage from './MainPage';

import {createBrowserRouter, RouterProvider, Outlet} from "react-router-dom";




  function App(props) {
    if (props.attendees === undefined) {
      return null;
    }
  
    const router = createBrowserRouter([
      {
        path: "/",
        element: (
          <>
            <Nav />
            <div className="container">
              <Outlet />
            </div>
          </>
        ),
        children: [
          { index: true, element: <MainPage /> },
          {
            path: "locations",
            children: [ { path: "new", element: <LocationForm /> }, ]
          },
          {
            path:"attendees", 
            // element: <AttendeesList attendees={props.attendees},
            children: [{path: "new", element: <AttendeeSignUp />},
                       {path: "", element: <AttendeesList attendees={props.attendees} />}, ] 
          },
          {
            path: "conferences",
            children: [ { path: "new", element: <ConferenceForm /> }, ]
          },
          {
            path: "presentations",
            children: [ { path: "new", element: <PresentationForm /> }, ]
          },
          
          
        ],
      },
    ])
  
    return <RouterProvider router={router} />;
  }
  
 
    
    
          
          // <Route path="/conferences/new" element={<ConferenceForm />} /> 
         
            
          
        
    
  


export default App;