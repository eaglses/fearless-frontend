import React, {useEffect, useState} from 'react';



function AttendeeSignUp (props) {
  
  const [conferences, setConferences] = useState([]);
  const [conference, setConference] = useState('');
     const [name, setName] = useState('');
     const [email, setEmail] = useState('');

     const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }
      const handleConference = (event) => {
        const selectedIndex = event.target.options.selectedIndex;
        const href = (event.target.options[selectedIndex].getAttribute('value'));
        setConference(href);
      }
      const handleEmail = (event) => {
        const value = event.target.value;
        setEmail(value);
      }
    const handleSubmit = async (event) => {
        event.preventDefault();
  
        const data = {};
  
        data.name = name;
        data.email = email;
        data.conference = conference;
     
        
    

        
        
        const locationUrl = `http://localhost:8001${conference}attendees/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);

        

        if (response.ok) {
            const newLocation = await response.json();
         
        
            setConference('');
            setName('');
            setEmail('');
                       
            
          }
    }
  

      

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
       const data = await response.json();
      setConferences(data.conferences);
   
      
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>It's Conferene Time! </h1>
            <p>Please choose which conference you'd like to attend.</p>
            <form onSubmit={handleSubmit} id="create-attendee-form">
            <div className="col-12">
                <select onChange={handleConference} required name="conference" id="conference" className="htmlForm-select" value={conference}>
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <p>Now, tell us about yourself.</p>
                <div className='row'>
                <div className="htmlForm-floating mb-3 col">
                  <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                </div>
                <div className="htmlForm-floating mb-3 col">
                  <input onChange={handleEmail} placeholder="please enter your email address" required type="email" name="email" id="email" className="form-control" value={email} />
                </div>
              </div>
              <button className="btn btn-primary">I'm going!</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default AttendeeSignUp;