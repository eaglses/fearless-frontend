window.addEventListener('DOMContentLoaded', async () => {
    function createCard(name, description, pictureUrl, starts, ends, location) {
        return `
        <div class="col">
          <div class="card, shadow-lg p-3 mb-5 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <p class="card-subtitle text-secondary"> ${location}</p>
             <ul class="list-group list-group-flush">
                 <li class="list-group-item"><p class="card-text">${description}</p> </li>
                 <li class="list-group-item">${starts}-${ends}</li>
             </ul>
            </div>
          </div>
        </div>
        `;
      }

      function stringToDate(string){
        let rtn = string.split("T");
        let date = rtn[0].split("-")
        return `${date[2]}/${date[1]}/${date[0]}`
      }


    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
        
      if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error('Response not ok');
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = stringToDate(details.conference.starts);
            const ends = stringToDate(details.conference.ends);
            const location = stringToDate(details.conference.location.name)
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            const column = document.querySelector("#row");
            column.innerHTML += html;

          }
        }
  
      }
    } catch (e) {
        console.log("error trip")
      
        return `
        <div class="col">
         <div class="alert alert-danger" role="alert">
            sorry there has been an error${error(e)}
         </div>
        </div>
        `
    }
  
  });